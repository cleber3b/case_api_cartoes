package com.br.casecartoes.controllers;

import com.br.casecartoes.models.Cliente;
import com.br.casecartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente){
        return clienteService.salvarCliente(cliente);
    }

    @GetMapping("/{idCliente}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente buscarPorID(@RequestBody @PathVariable(name = "idCliente") int idCliente){
        return clienteService.buscarPorID(idCliente);
    }



}
