package com.br.casecartoes.controllers;

import com.br.casecartoes.DTOs.PagamentoDTO;
import com.br.casecartoes.models.Pagamento;
import com.br.casecartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoDTO cadastrarPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO) {
        return pagamentoService.salvarPagamento(pagamentoDTO);
    }

    @GetMapping("/pagamentos/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoDTO> listarPagamentosPorIdCartao(@RequestBody @PathVariable(name = "idCartao") int idCartao) {
        return pagamentoService.listarPagamentosPorIdCartao(idCartao);

    }

}
