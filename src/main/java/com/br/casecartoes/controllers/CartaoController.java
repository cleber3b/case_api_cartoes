package com.br.casecartoes.controllers;

import com.br.casecartoes.DTOs.CartaoDTO;
import com.br.casecartoes.DTOs.CartaoResponseDTO;
import com.br.casecartoes.models.Cartao;
import com.br.casecartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoDTO cadastrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO) {
        return cartaoService.salvarCartao(cartaoDTO);
    }

    @GetMapping("/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoResponseDTO buscarPorID(@RequestBody @PathVariable(name = "idCartao") int idCartao){
        return cartaoService.buscarPorID(idCartao);
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoDTO ativarCartaoPeloNumero(@RequestBody @Valid CartaoDTO cartaoDTO, @PathVariable(name = "numero") @Valid int numero){
        return cartaoService.ativarCartaoPeloNumero(cartaoDTO, numero);
    }


}
