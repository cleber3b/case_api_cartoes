package com.br.casecartoes.services;

import com.br.casecartoes.DTOs.CartaoDTO;
import com.br.casecartoes.DTOs.CartaoResponseDTO;
import com.br.casecartoes.models.Cartao;
import com.br.casecartoes.models.Cliente;
import com.br.casecartoes.repositories.CartaoRepository;
import com.br.casecartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    public CartaoDTO salvarCartao(CartaoDTO cartaoDTO) {
        Cliente cliente = clienteRepository.findById(cartaoDTO.getClienteId());

        if (cliente != null) {
            Cartao cartao = new Cartao();
            cartao.setNumero(cartaoDTO.getNumero());
            cartao.setAtivo(false);
            cartao.setCliente(cliente);
            cartaoRepository.save(cartao);

            return this.transformarCartaoEmCartaoDTO(cartao);
        } else {
            throw new RuntimeException("Cliente não existe");
        }
    }

    public CartaoResponseDTO buscarPorID(int idCartao) {
        Cartao cartao = cartaoRepository.findById(idCartao);

        if(cartao != null){
            return this.transformarCartaoEmCartaoResponseDTO(cartao);
        }else {
            throw new RuntimeException("Cartão não foi encontrado");
        }
    }

    public CartaoDTO ativarCartaoPeloNumero(CartaoDTO cartaoDTO, int numero) {
        Cartao cartao = cartaoRepository.findByNumero(numero);

        if(cartao != null){
            cartao.setAtivo(cartaoDTO.isAtivo());
            cartaoRepository.save(cartao);

            return this.transformarCartaoEmCartaoDTO(cartao);
        }else{
            throw new RuntimeException("Cartão inválido ou inexistente para ser Ativado.");
        }

    }

    private CartaoDTO transformarCartaoEmCartaoDTO(Cartao cartao) {
        CartaoDTO cartaoDTO = new CartaoDTO();

        cartaoDTO.setId(cartao.getId());
        cartaoDTO.setNumero(cartao.getNumero());
        cartaoDTO.setAtivo(cartao.getativo());
        cartaoDTO.setClienteId(cartao.getCliente().getId());

        return cartaoDTO;
    }

    private CartaoResponseDTO transformarCartaoEmCartaoResponseDTO(Cartao cartao) {
        CartaoResponseDTO cartaoResponseDTO = new CartaoResponseDTO();

        cartaoResponseDTO.setId(cartao.getId());
        cartaoResponseDTO.setNumero(cartao.getNumero());
        cartaoResponseDTO.setClienteId(cartao.getCliente().getId());

        return cartaoResponseDTO;
    }


}
