package com.br.casecartoes.services;

import com.br.casecartoes.models.Cliente;
import com.br.casecartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscarPorID(int idCliente) {
        Cliente cliente = clienteRepository.findById(idCliente);

        if(cliente != null){
            return cliente;
        }else {
            throw new RuntimeException("Cliente não foi encontrado");
        }
    }
}
