package com.br.casecartoes.services;

import com.br.casecartoes.DTOs.PagamentoDTO;
import com.br.casecartoes.erros.TratamentoErros;
import com.br.casecartoes.models.Cartao;
import com.br.casecartoes.models.Pagamento;
import com.br.casecartoes.repositories.CartaoRepository;
import com.br.casecartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoRepository cartaoRepository;

    public PagamentoDTO salvarPagamento(PagamentoDTO pagamentoDTO) {
        Cartao cartao = cartaoRepository.findById(pagamentoDTO.getCartao_id());

        if (cartao != null) {
            if(cartao.getativo()) {
                Pagamento pagamento = new Pagamento();

                pagamento.setDescricao(pagamentoDTO.getDescricao());
                pagamento.setValor(pagamentoDTO.getValor());
                pagamento.setCartao(cartao);

                pagamentoRepository.save(pagamento);

                return this.transformarPagamentoEmPagamentoDTO(pagamento);
            }else{
                throw new RuntimeException("Cartão não ativado! Ative pelo no AppItau ou no site https://www.itau.com.br/atendimento-itau/para-voce/");
            }
        } else {
            throw new RuntimeException("Cartão não encontrado para efetuar o Pagamento.");
        }
    }

    public List<PagamentoDTO> listarPagamentosPorIdCartao(int idCartao) {
        Cartao cartao = cartaoRepository.findById(idCartao);

        if (cartao != null) {
            return this.transformarListaDePagamentosEmPagamentoDTO((List<Pagamento>) pagamentoRepository.findAllByCartaoId(idCartao));
        } else {
            throw new RuntimeException("Cartão não encontrado.");
        }
    }

    private PagamentoDTO transformarPagamentoEmPagamentoDTO(Pagamento pagamento) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO();

        pagamentoDTO.setId(pagamento.getId());
        pagamentoDTO.setCartao_id(pagamento.getCartao().getId());
        pagamentoDTO.setDescricao(pagamento.getDescricao());
        pagamentoDTO.setValor(pagamento.getValor());

        return pagamentoDTO;
    }

    private List<PagamentoDTO> transformarListaDePagamentosEmPagamentoDTO(List<Pagamento> pagamentos) {
        List<PagamentoDTO> listaPagamentoDTO = new ArrayList<>();
        PagamentoDTO pagamentoDTO;

        for (Pagamento pagamento : pagamentos) {
            pagamentoDTO = new PagamentoDTO();

            pagamentoDTO.setId(pagamento.getId());
            pagamentoDTO.setCartao_id(pagamento.getCartao().getId());
            pagamentoDTO.setDescricao(pagamento.getDescricao());
            pagamentoDTO.setValor(pagamento.getValor());

            listaPagamentoDTO.add(pagamentoDTO);
        }

        return listaPagamentoDTO;
    }
}
