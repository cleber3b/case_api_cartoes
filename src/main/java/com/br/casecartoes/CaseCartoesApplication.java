package com.br.casecartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaseCartoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaseCartoesApplication.class, args);
	}

}
