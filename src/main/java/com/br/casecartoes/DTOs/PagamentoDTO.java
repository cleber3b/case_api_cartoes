package com.br.casecartoes.DTOs;

import javax.validation.constraints.*;

public class PagamentoDTO {

    private int id;

    private int cartao_id;

    @NotBlank(message = "Descricao precisa ser preenchida.")
    @NotNull(message = "Descricao precisa ser preenchida.")
    @Size(min = 5, message = "Descricao precisa ser preenchida com no mínio 5 letras.")
    private String descricao;

    @NotNull(message = "Valor não pode ser nulo ou vazio.")
    @DecimalMin(value = "1.0", message = "Valor precisa ser maior que R$1.00.")
    @Digits(integer = 6, fraction = 2, message = "Valor fora do padrão.")
    private Double valor;

    public PagamentoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
