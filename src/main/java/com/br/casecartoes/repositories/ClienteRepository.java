package com.br.casecartoes.repositories;

import com.br.casecartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Cliente findById(int id);

}
