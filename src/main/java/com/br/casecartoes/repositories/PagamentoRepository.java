package com.br.casecartoes.repositories;

import com.br.casecartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    Iterable<Pagamento> findAllByCartaoId(int idCartao);
}
