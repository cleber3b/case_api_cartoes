package com.br.casecartoes.repositories;

import com.br.casecartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Cartao findById(int id);

    Cartao findByNumero(int numero);
}
