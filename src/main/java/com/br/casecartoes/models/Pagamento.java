package com.br.casecartoes.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "pagamentos")
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Descricao precisa ser preenchida.")
    @NotNull(message = "Descricao precisa ser preenchida.")
    @Size(min = 5, message = "Descricao precisa ser preenchida com no mínio 5 letras.")
    private String descricao;

    @NotNull(message = "Valor não pode ser nulo ou vazio.")
    @DecimalMin(value = "1.0", message = "Valor precisa ser maior que R$1.00.")
    @Digits(integer = 6, fraction = 2, message = "Valor fora do padrão.")
    private Double valor;

    @ManyToOne
    private Cartao cartao;

    public Pagamento() {
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
